# AWS Elastic Container Registry module

Terraform module which creates an ECR repository on AWS.

These types of resources are supported:

* Repository
* Image lifecycle policy

# Usage 

```hcl
module "ecr" {
  source           = "git::https://https://bitbucket.org/v44_crew/terraform-ecr.git?ref=tags/v1.0.0"
  name             = "${var.name}"
  repositorypolicy = data.aws_iam_policy_document.allowlocals.json
  image_tag_mutability = "${var.image_tag_mutability}
}
```

Please note that the `ref` argument in the `source` URL should always be set to the latest version value. You can find the latest version by che

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| tags | Implements the common tags scheme | map | n/a | yes |
| name | The name of the registry | string | n/a | yes |
| repositorypolicy | Pass this variable a Json Policy | string | n/a | yes |
| image_tag_mutability | The tag mutability setting for the repository | string | IMMUTABLE | yes |

## Outputs

| Name | Description |
|------|-------------|
| ecr\_arn | The Amazon resource name for the repository |
| ecr\_repo\_name | The name of the repository |
| ecr\_repository\_url | The URL of your new registry |
