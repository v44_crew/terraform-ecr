resource "aws_ecr_repository" "repository" {
  name = "${var.name}"
  image_tag_mutability = "${var.image_tag_mutability}"
}

resource "aws_ecr_lifecycle_policy" "cleanup" {
  repository = "${aws_ecr_repository.repository.name}"
  policy     = "${file("${path.module}/rotation_rules.json")}"
}
