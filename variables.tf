variable "name" {
  type        = "string"
  description = "The name of the registry"
}

variable "image_tag_mutability" {
  type        = "string"
  description = "The tag mutability setting for the repository. Can either be 'MUTABLE' or 'IMMUTABLE'"
  default     = "IMMUTABLE"
}

variable "repositorypolicy" {
  type        = "string"
  description = "Pass this variable a Json Policy"
}

variable "tags" {
  description = "Implements the common tags scheme"
}
