## v1.0.2 - 2019-10-18

- Add image tag muteability

## v1.0.1 - 2019-09-17

- Change common_tags to tags

## v1.0.0 - 2019-09-17

- Initial commit 